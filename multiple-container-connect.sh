#!/bin/bash
#Author: Ghiles KHEDDACHE

set_iptables()
{

        public_ip=$1
        container_name=$2
        container_path=$3


        echo "IP: $ip_exist found in Network configuration"
        echo "-------------------- --------------------------|"
        private_ip=$(sudo lxc-info -n $container_name -P $container_path | awk -F ':' '/IP/ {print $2}'|column -t)
        echo " "
        echo "IP $private_ip was getting from containner"
        echo "setting IPTABLES config..."
        #A ne pas décommenter
        #iptables -F; iptables -t nat -F; iptables -t mangle -F;
        sudo iptables -t nat -A POSTROUTING -s $private_ip -j SNAT --to-source $public_ip
        sudo iptables -t nat -A PREROUTING -d $public_ip -j DNAT --to-destination $private_ip
        echo "IPTABLES config is down, the container $container_name is now connected to the network!"
}


start_lxc()
{

        # check if container is running or start it

        contstatus=$(sudo lxc-info -n $1 -P $2 | awk -F ':' '/State/ {print $2}'|column -t)
        #### remplacer le if par while.
        if [ $contstatus == "RUNNING" ] ; then
                echo "$container_name is $contstatus"

        else
                echo "le conteneur $1 n'est pas allumé ou n'existe pas ---> start it"

                sudo lxc-start -n $1 -P $2 
                sleep 15
        fi
}

####################################
###             MAIN             ###
####################################

cat $1 | while IFS=';' read CNT IP PTH; do


        ip_exist=$(ip a | awk -F ' ' '/inet / {print $2}'| grep -w "$IP")
        if [ -n "$ip_exist" ] ; then

        # start_lxc
          start_lxc "$CNT" "$PTH"

        # set iptables
          set_iptables "$IP" "$CNT" "$PTH"

        else

          echo "l'adresse ip n'existe pas ou n'est pas rattaché correctement"

        fi


done

